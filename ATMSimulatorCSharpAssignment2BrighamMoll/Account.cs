﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// This enum has a list of possible account types arranged by number.
    /// </summary>
    public enum AccountType
    {
        Chequing = 1,
        Savings
    }

    /// <summary>
    /// Each Account object stores information on an account such as balance, name, account number, etc. 
    /// This class is also responsible for directly depositing or withdrawing money when the bank tells it to.
    /// The account class will load and save accounts directly as well.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// _acctHolderName will store the name of the account's holder, used when saving their information.
        /// </summary>
        private string _acctHolderName;

        /// <summary>
        /// _acctNo will store the specific account number associated
        /// with a certain account, used to identify it on login.
        /// </summary>
        private int _acctNo;

        /// <summary>
        /// _annualIntrRate will store the specific account's annual interest rate for calculating a future balance.
        /// </summary>
        private float _annualIntrRate;

        /// <summary>
        /// _balance will store the specific account's balance, used in depositing and withdrawing money.
        /// </summary>
        private double _balance;

        /// <summary>
        /// The number of transactions that have taken place on this account.
        /// </summary>
        private int _transNum;

        /// <summary>
        /// The list of transactions associated with this account.
        /// </summary>
        private List<Transaction> _transList;

        /// <summary>
        /// Initializes account objects with their attributes.
        /// This constructor uses an entered account number and holder name from the user to generate a new account.
        /// For loading accounts in files, there are default account numbers and names so that the method can be used without user input.
        /// </summary>
        /// <param name="acctNo"> The account number. </param>
        /// <param name="acctHolderName"> The name of the account holder. </param>
        public Account(int acctNo = -1, string acctHolderName = "")
        {
            // Account number is set to one given by the user.
            _acctNo = acctNo;
            // Account holder name is set to one given by the user.
            _acctHolderName = acctHolderName;
            // Balance on the new account is set to 0. (Initialized.)
            _balance = 0.0F;
            // Annual interest rate on the new account is set to 0. (Initialized.)
            _annualIntrRate = 0.0F;
            // Transaction number is set to 0. (Initialized.)
            _transNum = 0;
            // Initialize the transaction list for the new account.
            _transList = new List<Transaction>();
        }

        /// <summary>
        /// The current account's number.
        /// </summary>
        public int AccountNumber
        {
            // _acctNo represents the account number returned.
            get { return _acctNo; }
            set { _acctNo = value; }
        }

        /// <summary>
        /// The current account's holder name.
        /// </summary>
        public string AcctHolderName
        {
            // _acctHolderName represents the account holder name returned.
            get { return _acctHolderName; }
            set { _acctHolderName = value; }
        }

        /// <summary>
        /// The current account's balance.
        /// </summary>
        public double Balance
        {
            // _balance represents the account balance returned.
            get { return _balance; }
        }

        /// <summary>
        /// The current account's annual interest rate.
        /// </summary>
        public virtual float AnnualIntrRate
        {
            // _annualIntrRate represents the annual interest rate returned.
            get { return (_annualIntrRate*100); }
            // Set the annual interest rate to the new interest rate percentage divided by 100.
            set { _annualIntrRate = ( value / 100f); }
        }

        /// <summary>
        /// The monthly interest rate of the account.
        /// </summary>
        public float MonthlyIntrRate
        {
            // Divides the annual interest rate by 12 to get the monthly interest rate, then returns.
            get { return (_annualIntrRate / 12); }
        }

        /// <summary>
        /// Deposit entered amount of money and return the new balance as long as deposit value is valid.
        /// </summary>
        /// <param name="amount"> The amount the user would like to deposit. </param>
        /// <param name="savingsAdded"> For a savings account, this number will be set to the extra amount added during deposit. This is for transaction records. </param>
        /// <returns> New balance after deposit. </returns>
        public virtual double Deposit(double amount, double savingsAdded = 0)
        {
            // Ensure that the amount being deposited is a positive value.
            if (amount < 0)
            {
                throw new InvalidTransactionException("Cannot deposit a negative value.");
            }

            // Add the deposited money to the current bank account balance.
            double _oldBalance = _balance;
            _balance += amount;

            // Record the transaction. If no transactions have taken place before, this is an account creation.
            if (_transNum == 0)
            {
                RecordTransaction(TransType.Creation, amount, _oldBalance);
            }
            else
            {
                RecordTransaction(TransType.Deposit, amount, _oldBalance, savingsAdded);
            }
            // _balance is the current balance on this account after deposit.
            // Return the new balance after depositing money.
            return _balance;
        }

        /// <summary>
        /// Withdraw entered amount of money and return the new balance as long as there is enough 
        /// money in the account.
        /// </summary>
        /// <param name="amount"> The amount the user would like to withdraw. </param>
        /// <returns> New balance after withdrawal. </returns>
        public virtual double Withdraw(double amount, bool chequing = false)
        {
            // Ensure that withdraw amount is not a negative number.
            if (amount < 0)
            {
                throw new InvalidTransactionException("Amount withdrawn cannot be negative.");
            }
            // Ensure that the amount withdrawn is less than or equal to the current balance.
            if (chequing == false)
            {
                if (amount > _balance)
                {
                    throw new InvalidTransactionException("Not enough available funds.");
                }
            }
            // Subtract withdrawn money from balance.
            double _oldBalance = _balance;
            _balance -= amount;

            // Record the transaction.
            RecordTransaction(TransType.Withdrawal, amount, _oldBalance);

            // _balance is the current balance on this account after withdrawal.
            // Return balance after withdrawal.
            return _balance;
        }

        /// <summary>
        /// Loads account information from a certain file.
        /// </summary>
        /// <param name="file"> The file containing account information. </param>
        public void Load(StreamReader acctFileReader)
        {
            // Read the account properties in the same order they were saved.
            // (Account number, holder name, balance, and annual interest rate.)
            _acctNo = int.Parse(acctFileReader.ReadLine());
            _acctHolderName = (acctFileReader.ReadLine());
            _balance = double.Parse(acctFileReader.ReadLine());
            _annualIntrRate = float.Parse(acctFileReader.ReadLine());

            // Read account type.
            string acctType = acctFileReader.ReadLine();

            // Read transaction number.
            _transNum = int.Parse(acctFileReader.ReadLine());
            // Check remaining lines for past transactions to add to transaction list.
            string nextTransaction = acctFileReader.ReadLine();
            while (nextTransaction != null)
            {
                // Since only the generated transaction print line is used for saving, loading,
                // and showing the user their transactions, the other parameters are not relevant.
                // (Hence being set to 0.)
                Transaction importedTransaction = new Transaction(TransType.Creation, 0, 0, 0, 0, 0, nextTransaction);
                // Add importedTransaction to transaction list in account.
                _transList.Add(importedTransaction);
                // Check to see if there is another transaction to be read. (If there isn't, the loop breaks.)
                nextTransaction = acctFileReader.ReadLine();
            }
            return;
        }

        /// <summary>
        /// Saves account information to a certain file.
        /// </summary>
        /// <param name="file"> The file to be saved with account information. </param>
        public void Save(StreamWriter acctFileWriter)
        {
            // Write the account properties, one per line in order.
            // (Account number, holder name, balance, and annual interest rate.)
            acctFileWriter.WriteLine(_acctNo);
            acctFileWriter.WriteLine(_acctHolderName);
            acctFileWriter.WriteLine(_balance);
            acctFileWriter.WriteLine(_annualIntrRate);
            // Determine account type and write it down using overridden ToString() method.
            acctFileWriter.WriteLine(ToString());

            // Lastly, record current transaction number and all past transaction generated lines.
            acctFileWriter.WriteLine(_transNum);
            // Go through each transaction object in account list and write out their generated lines.
            foreach (Transaction currentTransaction in _transList)
            {
                acctFileWriter.WriteLine(currentTransaction.TransLine);
            }

            return;
        }

        /// <summary>
        /// Records the current transaction by creating an object and adding it to the account's _transList.
        /// </summary>
        /// <param name="transType"> Type of transaction taking place. </param>
        /// <param name="amount"> The amount being withdrawn or deposited. </param>
        /// <param name="oldBalance"> The balance before the transaction took place. </param>
        /// <param name="savingsAdded"> For a savings account, this number will be set to the extra amount added during deposit. This is for transaction records. </param>
        public void RecordTransaction(TransType transType, double amount, double oldBalance, double savingsAdded = 0)
        {
            // Increment the transaction number.
            _transNum++;

            // Create transaction record object.
            Transaction newTransaction = new Transaction(transType, amount, oldBalance, _balance, _transNum, savingsAdded);

            // Add transaction record to account's list of transactions.
            _transList.Add(newTransaction);
        }

        /// <summary>
        /// Retrieves transactions from transaction list and outputs to console for viewing.
        /// </summary>
        public void RetrieveTransactions()
        {
            // For each transaction in the account's list, output its generated record line to the console.
            foreach (Transaction currentTransaction in _transList)
            {
                Console.WriteLine(currentTransaction.TransLine);
            }
        }

        /// <summary>
        /// For saving account files, this will allow the program to determine the type to write down.
        /// </summary>
        /// <returns> The account type. </returns>
        public override string ToString()
        {
            return "A";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// This class is a special version of the Account class used for created chequing accounts in the ATM.
    /// </summary>
    public class ChequingAccount : Account
    {
        /// <summary>
        /// This is the maximum overdraft value for a chequing account.
        /// </summary>
        private const double OVERDRAFT_LIMIT = 500.0;

        /// <summary>
        /// This is the maximum interest rate allowed for a chequing account.
        /// </summary>
        private const float MAX_INTEREST_RATE = 1.0f;

        /// <summary>
        /// ChequingAccount constructor that ensures that the base constructor is called.
        /// Default parameter values are provided for when accounts are loaded from saved files.
        /// </summary>
        /// <param name="acctNo"> The account number. </param>
        /// <param name="acctHolderName"> The name of the account holder. </param>
        public ChequingAccount(int acctNo = -1, string acctHolderName = "") :
            base(acctNo, acctHolderName)
        {
            // There is nothing to initialize in Chequing Account class.
            // Constructor is provided for accepting data from base class through base(...) call.
        }

        /// <summary>
        /// Change annual interest rate on an account, verifying that it is okay with a chequing account.
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage"> The annual interest rate as a percentage. </param>
        public override float AnnualIntrRate
        {
            set
            {
                // Ensure that the annual interest rate is valid for a chequing account.
                if (value > MAX_INTEREST_RATE)
                {
                    Console.WriteLine("The maximum interest rate you can have for a chequing account is 1%, sorry.");
                    // This value will alert the program to reprompt for an interest rate.
                    // (In the original program, accounts are created whether or not they have proper interest rates.)
                    base.AnnualIntrRate = (-1);
                }
                // Use base class to set annual interest rate.
                else
                {
                    base.AnnualIntrRate = value;
                }
            }
        }

        /// <summary>
        /// Withdraw entered amount of money and return the new balance, if money is available in account.
        /// (Follows chequing account rules.)
        /// </summary>
        /// <param name="amount"> The amount the user would like to withdraw. </param>
        /// <returns> New balance after withdrawal. </returns>
        public override double Withdraw(double amount, bool chequing = true)
        {
            // Negative number check done in base method.
            // Ensure that the amount withdrawn is not more than the overdraft limit and the balance of the account together.
            if (amount > (Balance + OVERDRAFT_LIMIT))
            {
                throw new InvalidTransactionException("Not enough funds to complete this withdrawal. Your overdraft limit is $500.");
            }
            // Withdraw using base class. In order to consider the overdraft limit, the chequing argument is set to true.
            // When true, it will ignore the usual (if (amount > Balance)) check.
            base.Withdraw(amount, chequing);

            // Return the account balance after the withdrawal.
            return Balance;
        }

        /// <summary>
        /// For saving account files, this will allow the program to determine the type to write down.
        /// </summary>
        /// <returns> The account type. </returns>
        public override string ToString()
        {
            return "C";
        }
    }
}

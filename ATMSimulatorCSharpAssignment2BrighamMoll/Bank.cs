﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// The Bank class loads, saves, finds, and creates bank accounts. 
    /// It provides contact with the ATM to log users in and allow them to do banking with existing accounts.
    /// (Has a list of accounts.)
    /// </summary>
    public class Bank
    {
        /// <summary>
        /// This list will be populated with all of the bank accounts in the system so far.
        /// Using this list, the user will be able to login to any account that exists and use it for banking.
        /// </summary>
        private List<Account> _accountList;

        /// <summary>
        /// Default account numbers start from this one. (100-109)
        /// </summary>
        private const int DEFAULT_ACCT_NO_START = 100;

        /// <summary>
        /// Name of the folder used to hold all account files.
        /// </summary>
        private const string BANKING_DATA_FOLDER = "BankingData";

        /// <summary>
        /// Prefix used for account files storing regular account data.
        /// </summary>
        private const string ACCT_FILE_PREFIX = "acct";

        /// <summary>
        /// Prefix used for account files storing chequing account data.
        /// </summary>
        private const string CHQ_ACCT_FILE_PREFIX = "chqacct";

        /// <summary>
        /// Prefix used for account files storing savings account data.
        /// </summary>
        private const string SAV_ACCT_FILE_PREFIX = "savacct";

        /// <summary>
        /// Initializes the account list for storing all bank accounts.
        /// </summary>
        public Bank()
        {
            // Initialize account list.
            _accountList = new List<Account>();
        }

        /// <summary>
        /// Load account data for all bank accounts from the BankingData directory in the current directory.
        /// </summary>
        public void LoadAccountData()
        {
            // Check if a data directory exists for past accounts.
            string saveDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\SavedAccountData";
            if ((Directory.Exists(saveDirectory)) == true)
            {
                // If it exists, get a list of the files in the directory.
                string[] savedFiles = Directory.GetFiles(saveDirectory);
                // For each file, create an account object of appropriate type and load the information from the file to do so.
                foreach (string file in savedFiles)
                {
                    StreamReader fileLoaded = new StreamReader(file);
                    // Determine account type based on file name and create proper account.
                    if ( file.StartsWith(saveDirectory + "\\C") )
                    {
                        ChequingAccount loadedAccount = new ChequingAccount();
                        // Read account data into object.
                        loadedAccount.Load(fileLoaded);
                        // Add each account to the list of accounts.
                        _accountList.Add(loadedAccount);
                    }
                    else if (file.StartsWith(saveDirectory + "\\S") )
                    {
                        SavingsAccount loadedAccount = new SavingsAccount();
                        // Read account data into object.
                        loadedAccount.Load(fileLoaded);
                        // Add each account to the list of accounts.
                        _accountList.Add(loadedAccount);
                    }
                    else
                    {
                        Account loadedAccount = new Account();
                        // Read account data into object.
                        loadedAccount.Load(fileLoaded);
                        // Add each account to the list of accounts.
                        _accountList.Add(loadedAccount);
                    }

                    // Close the files.
                    fileLoaded.Close();
                }
            }
            // If there are still no accounts in the system, create the default accounts.
            if (_accountList.Count == 0)
            {
                CreateDefaultAccounts();
            }
        }

        /// <summary>
        /// Save account data for all bank accounts to the BankingData directory in the current directory.
        /// Each account is saved in a seperate file.
        /// </summary>
        public void SaveAccountData()
        {
            // If there is no current data directory, create one.
            string saveDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\SavedAccountData";
            if ((Directory.Exists(saveDirectory)) == false)
            {
                Directory.CreateDirectory(saveDirectory);
            }
            // For each account in the list, create an appropriate account file with saved information in the new data directory.
            foreach (Account currentAccount in _accountList)
            {
                // Determine account type for file name.
                string acctType = currentAccount.ToString();
                // Create files in account data folder.
                string filename = (saveDirectory + $"\\{acctType}{currentAccount.AccountNumber}.acct");
                // Save data to files.
                StreamWriter savefile = new StreamWriter(filename, false);
                currentAccount.Save(savefile);
                // Close all the files.
                savefile.Close();
            }

            return;
        }

        /// <summary>
        /// Create 10 bank accounts with predefined IDs and balances. 
        /// (Only created if there is no account data found to load.)
        /// </summary>
        public void CreateDefaultAccounts()
        {
            // Create 10 default accounts according to required properties, adding each to account list.
            for (int defAcctNum = 0; defAcctNum < 10; defAcctNum++)
            {
                Account newDefAcct = new Account(DEFAULT_ACCT_NO_START + defAcctNum, $"Default Account{defAcctNum.ToString()}");
                newDefAcct.Deposit(100);
                newDefAcct.AnnualIntrRate = 2.5f;

                _accountList.Add(newDefAcct);
            }


        }

        /// <summary>
        /// Returns account object if account number entered is found.
        /// Otherwise, null is returned.
        /// </summary>
        /// <param name="acctNo"> Account number entered to be used for finding the account object. </param>
        /// <returns> Returns the account object when successful, otherwise returns null. </returns>
        public Account FindAccount(int acctNo)
        {
            // Check all accounts for one with entered number, if found, return the account object appropriately.
            foreach (Account acct in _accountList)
            {
                // If number belongs to a real account, return the current account.
                if (acct.AccountNumber == acctNo)
                {
                    return acct;
                } 
            }
            // Otherwise, no account was found, and null is returned.
            return null;
        }

        /// <summary>
        /// Prompt user until they enter a valid account number for a new account being created.
        /// </summary>
        /// <returns> Returns valid account number when successfully generated. </returns>
        public int DetermineAccountNumber()
        {
            while (true)
            {
                try
                {
                    // Ask user to enter an account number or press enter to cancel.
                    Console.WriteLine("Please enter the account number [100 - 1000] or press [ENTER] to cancel: ");
                    string acctNoInput = Console.ReadLine();

                    // If nothing is entered, cancel.
                    if (acctNoInput.Length == 0)
                    {
                        throw new OperationCanceledException();
                    }
                    // Ensure that the account number entered fits in the range of possible account numbers.
                    int acctNo = int.Parse(acctNoInput);
                    if (acctNo < 100 || acctNo > 1000)
                    {
                        throw new InvalidValueException("The account number must be between 100 and 1000.");
                    }
                    // Ensure that the account number entered is not already taken by another account.
                    foreach (Account account in _accountList)
                    {
                        if (acctNo == account.AccountNumber)
                        {
                            throw new InvalidValueException("That account number has already been taken, sorry.");
                        }
                    }
                    // Return successfully generated account number if nothing goes wrong.
                    return acctNo;
                }
                catch(FormatException)
                {
                    Console.WriteLine("That entry was invalid. Please try again.");
                }
                catch(InvalidValueException err)
                {
                    Console.WriteLine(err.Message);
                }
            }
        }

        /// <summary>
        /// Create and store a new account object with required attributes.
        /// </summary>
        /// <param name="clientName"> Entered name of client opening new account. </param>
        /// <param name="acctType"> Selected account type for new account. </param>
        /// <returns> Returns new account object that is created. </returns>
        public Account OpenAccount(string clientName, AccountType acctType)
        {
            // Prompt user to enter an account number.
            int acctNo = DetermineAccountNumber();

            // Create and store an account object with required attributes.
            // Then, add the new account to the list of current accounts.
            // Finally, return the account object so that other properties can be set.
            if (acctType == AccountType.Chequing)
            {
                ChequingAccount newAccount = new ChequingAccount(acctNo, clientName);
                _accountList.Add(newAccount);
                return newAccount;
            }
            else if (acctType == AccountType.Savings)
            {
                SavingsAccount newAccount = new SavingsAccount(acctNo, clientName);
                _accountList.Add(newAccount);
                return newAccount;
            }
            else
            {
                // This code should never be used. This would be an error.
                return null;
            }
        }
    }
}

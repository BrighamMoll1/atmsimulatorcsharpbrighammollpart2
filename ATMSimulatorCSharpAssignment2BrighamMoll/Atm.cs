﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// This enum has a list of all the main menu options expressed as integers.
    /// </summary>
    public enum MainMenuOption
    {
        SelectAccount = 1,
        CreateAccount,
        ExitAtmApplication
    }

    /// <summary>
    /// This enum has a list of all the account menu options expressed as integers.
    /// </summary>
    public enum AccountMenuOption
    {
        CheckBalance = 1,
        Withdraw,
        Deposit,
        DisplayTransactions,
        ExitAccount
    }

    /// <summary>
    /// The Atm class is the direct user interface for the ATM, showing them menus and options for what to do.
    /// After the user selects what they want to do, the ATM will work with the rest of the system to
    /// carry out orders. A user can create accounts, withdraw, or deposit throught the Atm UI.
    /// </summary>
    public class Atm
    {
        /// <summary>
        /// _bank object created to use the Bank class and establish access to the banking accounts.
        /// </summary>
        private Bank _bank;

        /// <summary>
        /// The Atm class contructor initializes the bank object.
        /// </summary>
        /// <param name="bank"> The bank managing the bank account objects that the ATM works with. </param>
        public Atm(Bank bank)
        {
            // Initialize the bank object.
            _bank = bank;

        }

        /// <summary>
        /// Displays ATM menu for user to select options and manage their account.
        /// </summary>
        public void Start()
        {
            // Display the menu until the user chooses to leave the application.
            while (true)
            {
                // Display the main menu and allow user to select an option. Perform appropriate actions.
                MainMenuOption selectedOption = ShowMainMenu();

                switch(selectedOption)
                { 
                    // If the user chooses to select an account, run the selectAccount() method.
                    case MainMenuOption.SelectAccount:
                        Account acct = OnSelectAccount();
                        if (acct != null)
                        {
                            // If the selected account exists, allow the user to manage the account.
                            OnManageAccount(acct);
                        }
                        break;

                    // If the user chooses to open an account, run the onCreateAccount() method.
                    case MainMenuOption.CreateAccount:
                        OnCreateAccount();
                        break;
                    // If the user chooses to exit the application, save the account data, then close the program.
                    case MainMenuOption.ExitAtmApplication:
                        _bank.SaveAccountData();
                        return;
                    // If none of the options are selected, prompt the user again for a valid option.
                    default:
                        Console.WriteLine("Please enter a valid menu option.");
                        break;
                }
            }
        }

        /// <summary>
        /// Displays main menu and asks the user to select an option. Taking the input, it makes sure that
        /// the user enters a number, but does not check if it is in the menu.
        /// </summary>
        /// <returns> This returns an integer representing the menu option chosen by the user. </returns>
        private MainMenuOption ShowMainMenu()
        {
            // Show the actual text for the main menu of the application, and ask for an option input.
            while (true)
            {
                try
                {
                    // If an option is selected, return it.
                    Console.WriteLine("\nMain Menu\n\n1: Select Account\n2: Create Account\n3: Exit\n\nEnter a choice: ");
                    int userOption = int.Parse(Console.ReadLine());
                    return ((MainMenuOption)userOption);
                }
                catch(FormatException)
                {
                    // Otherwise, prompt the user again for a valid menu entry.
                    Console.WriteLine("Please enter a valid menu option.\n");
                }
            }

        }

        /// <summary>
        /// Displays account menu and asks the user to select an option. Taking the input, it makes sure that
        /// the user enters a number, but does not check if it is in the menu.
        /// </summary>
        /// <returns> This returns an integer representing the menu option chosen by the user. </returns>
        private AccountMenuOption ShowAccountMenu()
        {
            while (true)
            {
                try
                {
                    // Show the actual text for the account menu, and ask for an option input.
                    Console.WriteLine("\nAccount Menu\n\n1: Check Balance\n2: Withdraw\n3: Deposit\n4: Display Account Transactions\n5: Exit\n\nEnter a choice: ");
                    int userOption = int.Parse(Console.ReadLine());
                    // If an option is selected, return it.
                    return ((AccountMenuOption)userOption);
                }
                catch (FormatException)
                {
                    // Otherwise, prompt the user again for a valid menu entry.
                    Console.WriteLine("Please enter a valid menu option.\n");
                }
            }
        }

        /// <summary>
        /// This method will prompt the user for all account information including account type before 
        /// creating and adding the account object to the bank.
        /// </summary>
        private void OnCreateAccount()
        {
            while (true)
            {
                try
                {
                    // Get the name of the account holder for the new account, from the user.
                    string clientName = PromptForClientName();

                    // Get the initial deposit to the new account, from the user.
                    double initDepositAmount = PromptForDepositAmount();

                    // Get the account type for the new account, from the user.
                    AccountType acctType = PromptForAccountType();

                    // Get the annual interest rate for the new account, from the user.
                    float annIntrRate = PromptForAnnualIntrRate();

                    // Create the new account with the information gathered from the user, setting account properties.
                    Account newAccount = _bank.OpenAccount(clientName, acctType);
                    newAccount.Deposit(initDepositAmount);
                    newAccount.AnnualIntrRate = annIntrRate;

                    // If annual interest rate is returned as -1, the rate was not allowed in the account type.
                    // The account must be reprompted for annual interest rate.
                    while (newAccount.AnnualIntrRate == -1)
                    {
                        annIntrRate = PromptForAnnualIntrRate();
                        newAccount.AnnualIntrRate = annIntrRate;
                    }

                    // The new account has now been successfully created, so return the user to the main menu.
                    return;
                }
                catch(FormatException)
                {
                    Console.WriteLine("That entry was invalid. Please try again.");
                }
                catch(OperationCanceledException err)
                {
                    // User has cancelled account creation.
                    Console.WriteLine(err.Message + "\n");
                    return;
                }
                catch(InvalidValueException err)
                {
                    Console.WriteLine(err.Message + "\n");
                }
            }
        }

        /// <summary>
        /// Asks user for account number. If they do not enter anything, or the entered number is not accociated
        /// with an account, the operation cancels and returns the user to the main menu. If the account exists,
        /// the method will return the account object specified.
        /// </summary>
        /// <returns> Returns specified account object if login is successful. </returns>
        private Account OnSelectAccount()
        {
            while (true)
            {
                try
                {
                    // Prompt user for account ID to log in.
                    Console.WriteLine("Please enter your account ID or press [ENTER] to cancel: ");
                    String acctNoInput = Console.ReadLine();

                    // If the user enters nothing, cancel the operation and return to the main menu.
                    if (acctNoInput.Length == 0)
                    {
                        return null;
                    }

                    // Convert the user's entered number string into an int.
                    int acctNo = int.Parse(acctNoInput);

                    // Search for the bank account specified by the user. If found, log them in.
                    // acct is the Account object being logged into by the user.
                    Account acct = _bank.FindAccount(acctNo);
                    if (acct != null)
                    {
                        return acct;
                    }
                    // Otherwise, tell the user that the account does not exist as specified. Return to prompting for ID.
                    else
                    {
                        Console.WriteLine("The account was not found. Please select another account.");
                    }
                }
                catch(FormatException)
                {
                    // An invalid account number was entered.
                    Console.WriteLine("Please enter a valid account number. (e.g. 100)\n");
                }
            }
        }

        /// <summary>
        /// Allows the user to manage an account by executing operations on it such as depositing or withdrawing.
        /// </summary>
        /// <param name="account"> The account object to be managed by the user. </param>
        private void OnManageAccount(Account account)
        {
            while (true)
            {
                // Show the account menu to the user and accept an option choice.
                AccountMenuOption selAcctMenuOpt = ShowAccountMenu();

                switch (selAcctMenuOpt)
                {
                    // If they choose to check their balance, run the OnCheckBalance() method.
                    case AccountMenuOption.CheckBalance:
                        {
                            OnCheckBalance(account);
                            break;
                        }
                    // If they choose to withdraw, run the OnWithdraw() method.
                    case AccountMenuOption.Withdraw:
                        {
                            OnWithdraw(account);
                            break;
                        }
                    // If they choose to deposit, run the OnDeposit() method.
                    case AccountMenuOption.Deposit:
                        {
                            OnDeposit(account);
                            break;
                        }
                    // If they choose to display their transactions, run the OnDisplayTransactions() method.
                    case AccountMenuOption.DisplayTransactions:
                        {
                            OnDisplayTransactions(account);
                            break;
                        }
                    // If they choose to exit, return to the main menu of the application.
                    case AccountMenuOption.ExitAccount:
                        {
                            return;
                        }
                    // Otherwise, prompt the user again for a proper menu input.
                    default:
                        {
                            Console.WriteLine("Please enter a valid menu option.");
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Prompts user for name of client. Cancels if 'Enter' key is pressed.
        /// </summary>
        /// <returns> Returns name of client entered by user. </returns>
        private string PromptForClientName()
        {
                // Prompt the user for a client name.
                Console.WriteLine("Please enter the client name or press [ENTER] to cancel: ");
                string clientName = Console.ReadLine();

                // If they do not enter anything, cancel the operation.
                if (clientName.Length == 0)
                {
                    throw new OperationCanceledException();
                }

                // clientName is the client name being entered by the user.
                return clientName;  
        }

        /// <summary>
        /// Prompts the user to enter an initial account balance on account creation and performs basic
        /// error checking.
        /// </summary>
        /// <returns> Returns the initial account balance as a float value. </returns>
        private double PromptForDepositAmount()
        {
            while (true)
            {
                try
                {
                    // Prompt user for initial account balance to be deposited.
                    Console.WriteLine("Please enter your initial account balance: ");
                    double initAmount = double.Parse(Console.ReadLine());

                    // Check to make sure that the input is valid, and equal to or more than 0. Return if so.
                    if (initAmount >= 0)
                    {
                        // initAmount is the initial balance deposited by the user on account creation.
                        return initAmount;
                    }
                    // If there is a problem, prompt the user again for a valid input.
                    else
                    {
                        Console.WriteLine("Cannot create an account with a negative initial balance. Please enter a valid amount.");
                    }
                }
                catch(FormatException err)
                {
                    Console.WriteLine("That entry was invalid. Please try again.");
                }
            }
        }

        /// <summary>
        /// Prompts the user to enter the annual interest rate for account creation, doing basic error checking
        /// of the input.
        /// </summary>
        /// <returns> Returns the annual interest rate requested by the client on account creation. </returns>
        private float PromptForAnnualIntrRate()
        {
            while (true)
            {
                try
                {
                    // Prompt user for annual interest rate on new account.
                    Console.WriteLine("Please enter the annual interest rate for this account: ");
                    float intrRate = float.Parse(Console.ReadLine());

                    // Ensure that the entered value is valid, and equal to or more than 0.
                    if (intrRate >= 0)
                    {
                        // intrRate is the annual interest rate requested by the client on account creation.
                        return intrRate;
                    }
                    // Otherwise, prompt the user again for a proper annual interest rate for their new account.
                    else
                    {
                        Console.WriteLine("Cannot create an account with a negative interest rate.");
                    }
                }
                catch(FormatException)
                {
                    Console.WriteLine("That entry was invalid. Please try again.");
                }
            }
        }

        /// <summary>
        /// Prompts the user to enter an account type for account creation, doing basic error checking.
        /// </summary>
        /// <returns> Returns the account type selected. </returns>
        private AccountType PromptForAccountType()
        {
            while (true)
            {
                // Prompt the user to select one of the allowed account types. (Chequing or Savings.)
                Console.WriteLine("Please enter the account type [c/s: chequing / savings]: ");
                // acctTypeInput is the account type selected for the new account.
                string acctTypeInput = (Console.ReadLine()).ToUpper();

                // Depending on selection, return proper account type.
                if (acctTypeInput == "C" || acctTypeInput == "CHEQUING" || acctTypeInput == "CHECKING")
                {
                    return AccountType.Chequing;
                }
                else if (acctTypeInput == "S" || acctTypeInput == "SAVINGS" || acctTypeInput == "SAVING")
                {
                    return AccountType.Savings;
                }
                // Ensure that the user selects a valid choice. If they do not, prompt them again for a valid choice.
                else
                {
                    Console.WriteLine("Answer not supported. Please enter one of the supported answers.");
                }
            }
        }

        /// <summary>
        /// Prints current account balance.
        /// </summary>
        /// <param name="account"> The currently logged in account. </param>
        private void OnCheckBalance(Account account)
        {
            // Find the current account balance and tell the user what it is.
            string balanceAsString = (Math.Round(account.Balance, 2)).ToString("0.00");
            Console.WriteLine($"The balance is {balanceAsString}.\n");
        }

        /// <summary>
        /// Prompts user for input and performs deposit of money, using basic error checking on input.
        /// </summary>
        /// <param name="account"> The currently logged in account. </param>
        private void OnDeposit(Account account)
        {
            while (true)
            { 
                try
                {
                    // Prompt user for an amount to deposit to their account.
                    Console.WriteLine("Please enter an amount to deposit or type [ENTER] to exit: ");
                    string inputAmount = Console.ReadLine();
                    // If they do not enter anything, cancel the operation.
                    if (inputAmount.Length > 0)
                    {
                        double amount = double.Parse(inputAmount);
                        // Try to deposit the money with the deposit() method. Entered values will be further error-checked there.
                        account.Deposit(amount);
                    }
                    // The user either did not enter anything or the deposit is complete. Leave the loop.
                    return;
                }
                catch(FormatException)
                {
                    // User entered an invalid amount.
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.\n");
                }
                catch(InvalidTransactionException err)
                {
                    // Account refused to deposit. Reason is in exception object.
                    Console.WriteLine(err.Message + "\n");
                }
            }
        }

        /// <summary>
        /// Prompts user for input and performs withdrawal of money, using basic error checking on input.
        /// </summary>
        /// <param name="account"> The currently logged in account. </param>
        private void OnWithdraw(Account account)
        {
            while (true)
            { 
                try
                {
                    // Prompt user for an amount to withdraw from their account.
                    Console.WriteLine("Please enter an amount to withdraw or type [ENTER] to exit: ");
                    string inputAmount = Console.ReadLine();

                    // If they do not enter anything, cancel the operation.
                    if (inputAmount.Length > 0)
                    {
                        double amount = double.Parse(inputAmount);

                        // Try to withdraw the money with the withdraw() method. Entered values will be further error-checked there.
                        account.Withdraw(amount);
                    }
                    // Withdrawing complete, or user entered nothing. Leave the loop.
                    return;
                }
                catch(FormatException)
                {
                    // User entered an invalid amount.
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.\n");
                }
                catch(InvalidTransactionException err)
                {
                    // The account refused to withdraw the entered amount. Reason is in exception object.
                    Console.WriteLine(err.Message + "\n");
                }
            }
        }

        /// <summary>
        /// Displays the current account's past transactions.
        /// </summary>
        /// <param name="account"> The currently logged in account. </param>
        private void OnDisplayTransactions(Account account)
        {
            // Use the RetrieveTransactions() method to output all of the transaction contents to the screen for the current account.
            Console.WriteLine("\nTransactions:\n");

            account.RetrieveTransactions();
        }

        /// <summary>
        /// Exits the program, saving the account data first.
        /// </summary>
        private void OnExit()
        {
            // App is shutting down, save all account data.
        }
    }
}

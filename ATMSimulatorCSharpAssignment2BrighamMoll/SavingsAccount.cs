﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// This class is a special version of the Account class used for created savings accounts in the ATM.
    /// </summary>
    public class SavingsAccount : Account
    {
        /// <summary>
        /// This ratio represents how much money is automatically deposited with every user deposit to the account.
        /// (For every dollar deposited, fifty cents are added for free.)
        /// </summary>
        private const double MATCHING_DEPOSIT_RATIO = 0.5;

        /// <summary>
        /// This number represents the minimum interest rate allowed on a savings account.
        /// </summary>
        private const float MIN_INTEREST_RATE = 3.0f;

        /// <summary>
        /// SavingsAccount constructor that ensures that the base constructor is called.
        /// Default parameter values are provided for when accounts are loaded from saved files.
        /// </summary>
        /// <param name="acctNo"> The account number. </param>
        /// <param name="acctHolderName"> The name of the account holder. </param>
        public SavingsAccount(int acctNo = 0, string acctHolderName = "") :
            base(acctNo, acctHolderName)
        {
            // There is nothing to initialize in Savings Account class.
            // Constructor is provided for accepting data from base class through base(...) call.
        }

        /// <summary>
        /// Change annual interest rate on an account, verifying that it is okay with a savings account.
        /// </summary>
        /// <param name="newAnnualIntrRatePercentage"> The annual interest rate as a percentage. </param>
        public override float AnnualIntrRate
        {
            set
            {
                // Ensure that the annual interest rate is valid for a savings account.
                if (value < MIN_INTEREST_RATE)
                {
                    Console.WriteLine("Your interest rate must be higher for a savings account. The minimum is 3%.");
                    // This value will alert the program to reprompt for an interest rate.
                    // (In the original program, accounts are created whether or not they have proper interest rates.)
                    base.AnnualIntrRate = (-1);
                }
                // Use base class to set annual interest rate.
                base.AnnualIntrRate = value;
            }
        }

        /// <summary>
        /// Deposit entered amount of money and return the new balance as long as deposit value is valid.
        /// (Follows savings account rules.)
        /// </summary>
        /// <param name="amount"> The amount the user would like to deposit. </param>
        /// <returns> New balance after deposit. </returns>
        public override double Deposit(double amount, double savings = 0)
        {

            // autoDepositTotal is the amount deposited plus the amount auto-added.
            double autoDepositTotal;

            // Calculate auto-deposit amount.
            double savingsAdded = MATCHING_DEPOSIT_RATIO * amount;
            autoDepositTotal = amount + savingsAdded;

            // Use base class to deposit.
            base.Deposit(autoDepositTotal, savingsAdded);

            // Return the balance.
            return Balance;
        }

        /// <summary>
        /// For saving account files, this will allow the program to determine the type to write down.
        /// </summary>
        /// <returns> The account type. </returns>
        public override string ToString()
        {
            return "S";
        }
    }
}

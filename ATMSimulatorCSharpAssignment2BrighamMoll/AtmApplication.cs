﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// The main class of the program that defines the program entry point.
    /// Creates bank, creates ATM, and starts ATM.
    /// </summary>
    class AtmApplication
    {
        /// <summary>
        /// The main method that starts the program.
        /// </summary>
        static void Main()
        {
            // Use exception handling to ensure the app does not crash.
            try
            {
                AtmApplication app = new AtmApplication();
                app.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error occurred with the following message: {e.Message}");
            }
        }

        /// <summary>
        /// Truly the main method of the application object.
        /// Runs when app is created, and will create bank/ATM objects before running the ATM.
        /// </summary>
        private void Run()
        {
            try
            {
                // Creates a bank for a more real-life life implementation.
                Bank bank = new Bank();
                // Load old account data.
                bank.LoadAccountData();

                // Create ATM and link it with the bank.
                Atm atm = new Atm(bank);


                // Start the ATM machine.
                atm.Start();
            }
            catch(Exception e)
            {
                Console.WriteLine($"An error occurred with the following message: {e.Message}");
            }
        }

    }
}

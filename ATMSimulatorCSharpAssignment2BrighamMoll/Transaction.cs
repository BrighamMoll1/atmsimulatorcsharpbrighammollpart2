﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMSimulatorCSharpAssignment2BrighamMoll
{
    /// <summary>
    /// Represents the type of transaction completed. Can be either ( Account ) Creation, Deposit, or Withdrawal.
    /// </summary>
    public enum TransType
    {
        Creation = 1,
        Deposit,
        Withdrawal
    }

    /// <summary>
    /// This class represents a single transaction on an account when withdrawing or depositing money.
    /// It is used to record all completed transactions on an account for viewing.
    /// </summary>
    class Transaction
    {
        /// <summary>
        /// Represents the type of transaction completed. Can be either ( Account ) Creation, Deposit, or Withdrawal.
        /// </summary>
        private TransType _transType;

        /// <summary>
        /// Represents the amount of money being withdrawn or deposited.
        /// </summary>
        private double _amount;

        /// <summary>
        /// Represents the balance before the transaction.
        /// </summary>
        private double _oldBalance;

        /// <summary>
        /// Represents the new balance after the transaction.
        /// </summary>
        private double _newBalance;

        /// <summary>
        /// The number of the transaction based on how many transactions have been completed.
        /// </summary>
        private int _transNum;

        /// <summary>
        /// The generated transaction line for recording this transaction.
        /// </summary>
        private string _transLine;

        /// <summary>
        /// For savings account deposits, this will store the extra deposited amount.
        /// </summary>
        private double _savingsAdded;

        /// <summary>
        /// This constructor creates a transaction object to be stored in an account's transaction list.
        /// Each transaction takes in parameters detailing what was done in the transaction.
        /// </summary>
        /// <param name="transType"> The type of transaction that took place. </param>
        /// <param name="amount"> The amount withdrawn/deposited. </param>
        /// <param name="oldBalance"> The balance before the transaction took place. </param>
        /// <param name="newBalance"> The balance after the transaction took place. </param>
        /// <param name="transNum"> The number of transactions that have occured on this account. </param>
        /// <param name="savingsAdded"> The bonus amount added during a savings account deposit. </param>
        /// <param name="importedTransLine"> If not null, this is printed line from an imported transaction. </param>
        public Transaction(TransType transType, double amount, double oldBalance, double newBalance, int transNum, double savingsAdded, string importedTransLine = null)
        {
            // Transaction type initialized.
            _transType = transType;
            // Amount withdrawn/deposited initialized.
            _amount = amount;
            // Balance before transaction initialized.
            _oldBalance = oldBalance;
            // Balance after transaction initialized.
            _newBalance = newBalance;
            // Number of transaction initialized.
            _transNum = transNum;
            // Initializes the savings account deposit added.
            _savingsAdded = savingsAdded;
            // Transaction line generated for this transaction.
            if (importedTransLine == null)
            {
                _transLine = GenerateTransactionLine();
            }
            // If transaction is imported, uses already generated line.
            else
            {
                _transLine = importedTransLine;
            }
        }

        /// <summary>
        /// Generates the line (string) to be recorded in a record of this transaction.
        /// </summary>
        /// <returns> The line to be recorded for this transaction. </returns>
        public string GenerateTransactionLine()
        {
            // Round each monetary amount properly to be displayed on the transactions.
            string amountAsString = (Math.Round(_amount, 2)).ToString("0.00");
            string oldBalanceAsString = (Math.Round(_oldBalance, 2)).ToString("0.00");
            string newBalanceAsString = (Math.Round(_newBalance, 2)).ToString("0.00");
            string savingsAddedAsString = (Math.Round(_savingsAdded, 2)).ToString("0.00");

            // Check what type of transaction is taking place before generating an appropriate line.
            if (_transType == TransType.Creation)
            {
                return $"({_transNum}) Account created with ${amountAsString} on balance.";
            }
            else if (_transType == TransType.Deposit)
            {
                if (_savingsAdded == 0)
                {
                    return $"({_transNum}) Deposited ${amountAsString}. (Balance = ${oldBalanceAsString} --> ${newBalanceAsString})";
                }
                else
                {
                    return $"({_transNum}) Deposited ${amountAsString}. (Balance = ${oldBalanceAsString} --> ${newBalanceAsString}) [An extra ${savingsAddedAsString} was added to this savings account.]";
                }
            }
            else if (_transType == TransType.Withdrawal)
            {
                return $"({_transNum}) Withdrew ${amountAsString}. (Balance = ${oldBalanceAsString} --> ${newBalanceAsString})";
            }
            else
            {
                // Error. The code should never arrive here.
                return "Transaction ERROR.";
            }
        }

        /// <summary>
        /// The transaction line to be recorded in records.
        /// </summary>
        public string TransLine
        {
            get { return _transLine; }
        }
    }
}
